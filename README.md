[![pipeline status](https://gitlab.com/rosco.peco/rusty-vm/badges/master/pipeline.svg)](https://gitlab.com/rosco.peco/rusty-vm/commits/master)

A simple Java-ish VM written in Rust.

This project is intented mainly as a vehicle for me to learn Rust, in a real-world-sized project.

