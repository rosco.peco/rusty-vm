/*
 * Simple example, Dump class to stdout.
 *
 * Copyright (c)2018 Ross Bamford <roscopeco AT gmail DOT com>
 */
extern crate classfile_parser;
extern crate java_vm;

use classfile_parser::parse_class;
use java_vm::loader::dump_class;

fn main() {
    match parse_class("java/bin/Test") {
        Ok(class) => dump_class(&class),
        Err(err)  => println!("Error: {}", err)
    }
}
