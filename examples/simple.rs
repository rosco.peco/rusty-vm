/*
 * Simple example, run method 1 from Test.class
 *
 * Copyright (c)2018 Ross Bamford <roscopeco AT gmail DOT com>
 */
extern crate classfile_parser;
extern crate java_vm;

use classfile_parser::parse_class;
use classfile_parser::ClassFile;
use java_vm::loader::ResolvedClass;
use java_vm::interpreter::Interpreter;

fn run_method_1(class: &ClassFile) -> Result<i64, String> {
    let rc = ResolvedClass::resolve(&class);
    Interpreter::run_to_result(&rc.methods[1])
}

fn main() {
    match parse_class("java/bin/com/roscopeco/javavm/tests/Test") {
        Ok(class) => match run_method_1(&class) {
            Ok(val) => println!("Code finished: returned {}", val),
            Err(msg) => println!("Runtime error: {}", msg)
        }
        Err(err)  => println!("Parse error: {}", err)
    }
}
