/*
 * java_vm interpreter
 *
 * Copyright (c)2018 Ross Bamford <roscopeco AT gmail DOT com>
 */
use ::loader::ResolvedClass;
use ::loader::ResolvedMethod;

mod opcodes;

pub struct Frame<'method> {
    pub method: &'method ResolvedMethod,
    pub locals: [i64; 10],
    pub stack: Vec<i64>,
    pub pc: u16
}

pub struct Interpreter<'vm> {
    pub resolved_classes: Vec<ResolvedClass>,
    pub frame_stack: Vec<Frame<'vm>>,
}

impl <'method> Frame<'method> {
    pub fn new(method: &ResolvedMethod) -> Frame {
        Frame {
            method,
            locals: [0; 10],
            stack: Vec::new(),
            pc: 0
        }
    }
}

impl <'vm> Interpreter<'vm> {
    pub fn new(method: &'vm ResolvedMethod) -> Interpreter<'vm> {
        Interpreter {
            resolved_classes: Vec::new(),
            frame_stack: vec![Frame::new(method)]
        }
    }

    pub fn step(&mut self) -> Result<Option<i64>, String> {
        let frame_stack = &mut self.frame_stack;

        match Interpreter::fetch_next_insn(frame_stack) {
            Some(insn) => match insn {
                opcodes::jvm::ICONST_M1     => Ok(opcodes::op_iconst_i(frame_stack, -1)),
                opcodes::jvm::ICONST_0      => Ok(opcodes::op_iconst_i(frame_stack, 0)),
                opcodes::jvm::ICONST_1      => Ok(opcodes::op_iconst_i(frame_stack, 1)),
                opcodes::jvm::ICONST_2      => Ok(opcodes::op_iconst_i(frame_stack, 2)),
                opcodes::jvm::ICONST_3      => Ok(opcodes::op_iconst_i(frame_stack, 3)),
                opcodes::jvm::ICONST_4      => Ok(opcodes::op_iconst_i(frame_stack, 4)),
                opcodes::jvm::ICONST_5      => Ok(opcodes::op_iconst_i(frame_stack, 5)),
                opcodes::jvm::ILOAD         => Ok(opcodes::op_iload(frame_stack)),
                opcodes::jvm::ILOAD_0       => Ok(opcodes::op_iload_i(frame_stack, 0)),
                opcodes::jvm::ILOAD_1       => Ok(opcodes::op_iload_i(frame_stack, 1)),
                opcodes::jvm::ILOAD_2       => Ok(opcodes::op_iload_i(frame_stack, 2)),
                opcodes::jvm::ILOAD_3       => Ok(opcodes::op_iload_i(frame_stack, 3)),
                opcodes::jvm::ISTORE        => Ok(opcodes::op_istore(frame_stack)),
                opcodes::jvm::ISTORE_0      => Ok(opcodes::op_istore_i(frame_stack, 0)),
                opcodes::jvm::ISTORE_1      => Ok(opcodes::op_istore_i(frame_stack, 1)),
                opcodes::jvm::ISTORE_2      => Ok(opcodes::op_istore_i(frame_stack, 2)),
                opcodes::jvm::ISTORE_3      => Ok(opcodes::op_istore_i(frame_stack, 3)),
                opcodes::jvm::IADD          => Ok(opcodes::op_iadd(frame_stack)),
                opcodes::jvm::IRETURN       => Ok(opcodes::op_ireturn(frame_stack)),
                op @ _                      => Err(opcodes::op_unknown(frame_stack, op))
            },
            None => Err("Code underflow".to_string())
        }
    }

    fn fetch_next_insn(frame_stack: &[Frame]) -> Option<u8> {
        match frame_stack.last() {
            Some(frame) => Some(frame.method.code[frame.pc as usize]),
            None => None
        }
    }

    pub fn run_to_result(method: &ResolvedMethod) -> Result<i64, String> {
        let mut interp = Interpreter::new(method);

        loop {
            match interp.step() {
                Ok(rv) => match rv {
                    Some(val) => return Ok(val),
                    None => continue
                },
                Err(msg) => return Err(msg)
            };
        }
    }
}


