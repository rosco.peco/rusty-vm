/*
 * java_vm opcode implementations
 *
 * Copyright (c)2018 Ross Bamford <roscopeco AT gmail DOT com>
 */
use interpreter::Frame;

pub fn op_iconst_i(frame_stack: &mut Vec<Frame>, value: i64) -> Option<i64> {
    let frame = frame_stack.last_mut().unwrap();
    frame.stack.push(value);
    frame.pc += 1;
    None
}

pub fn op_istore(frame_stack: &mut Vec<Frame>) -> Option<i64> {
    let idx = frame_stack.last_mut().unwrap().method.code[frame_stack.last().unwrap().pc as usize + 1];
    frame_stack.last_mut().unwrap().pc += 1;
    op_istore_i(frame_stack, idx)
}

pub fn op_istore_i(frame_stack: &mut Vec<Frame>, local: u8) -> Option<i64> {
    let frame = frame_stack.last_mut().unwrap();
    frame.locals[local as usize] = frame.stack.pop().unwrap();
    frame.pc += 1;
    None
}

pub fn op_iload(frame_stack: &mut Vec<Frame>) -> Option<i64> {
    let idx = frame_stack.last_mut().unwrap().method.code[frame_stack.last().unwrap().pc as usize + 1];
    frame_stack.last_mut().unwrap().pc += 1;
    op_iload_i(frame_stack, idx)
}

pub fn op_iload_i(frame_stack: &mut Vec<Frame>, local: u8) -> Option<i64> {
    let frame = frame_stack.last_mut().unwrap();
    frame.stack.push(frame.locals[local as usize]);
    frame.pc += 1;
    None
}

pub fn op_iadd(frame_stack: &mut Vec<Frame>) -> Option<i64> {
    let frame = frame_stack.last_mut().unwrap();
    let v2 = frame.stack.pop().unwrap();
    let v1 = frame.stack.pop().unwrap();
    frame.stack.push(v1 + v2);
    frame.pc += 1;
    None
}

pub fn op_ireturn(frame_stack: &mut Vec<Frame>) -> Option<i64> {
    let ret = frame_stack.last_mut().unwrap().stack.pop().unwrap();

    frame_stack.pop();

    match frame_stack.last_mut() {
        Some(caller) => {
            caller.stack.push(ret);
            None
        },
        None => Some(ret),
    }
}

pub fn op_unknown(frame_stack: &[Frame], op: u8) -> String {
    let frame = frame_stack.last().unwrap();
    format!("Unimplemented opcode: 0x{:02x} (@pc: 0x{:04x})", op, frame.pc)
}

pub mod jvm {
    pub const ICONST_M1         :u8         = 0x02;
    pub const ICONST_0          :u8         = 0x03;
    pub const ICONST_1          :u8         = 0x04;
    pub const ICONST_2          :u8         = 0x05;
    pub const ICONST_3          :u8         = 0x06;
    pub const ICONST_4          :u8         = 0x07;
    pub const ICONST_5          :u8         = 0x08;
    pub const ILOAD             :u8         = 0x15;
    pub const ILOAD_0           :u8         = 0x1a;
    pub const ILOAD_1           :u8         = 0x1b;
    pub const ILOAD_2           :u8         = 0x1c;
    pub const ILOAD_3           :u8         = 0x1d;
    pub const ISTORE            :u8         = 0x36;
    pub const ISTORE_0          :u8         = 0x3b;
    pub const ISTORE_1          :u8         = 0x3c;
    pub const ISTORE_2          :u8         = 0x3d;
    pub const ISTORE_3          :u8         = 0x3e;
    pub const IADD              :u8         = 0x60;
    pub const IRETURN           :u8         = 0xac;
}

#[cfg(test)]
mod tests {
    use loader::ResolvedMethod;
    use super::*;

    #[test]
    fn test_op_iconst_i() {
        let test_method = &ResolvedMethod::test();
        let frame_stack = &mut vec![Frame::new(test_method)];

        // pc is 0
        assert_eq!(0, frame_stack.last().unwrap().pc);

        let result = op_iconst_i(frame_stack, -1);
        assert_eq!(result, None);

        // pc is 1
        assert_eq!(1, frame_stack.last().unwrap().pc);

        op_iconst_i(frame_stack, 0);
        op_iconst_i(frame_stack, 1);
        op_iconst_i(frame_stack, 2);

        assert_eq!(2, frame_stack.last_mut().unwrap().stack.pop().unwrap());
        assert_eq!(1, frame_stack.last_mut().unwrap().stack.pop().unwrap());
        assert_eq!(0, frame_stack.last_mut().unwrap().stack.pop().unwrap());
        assert_eq!(-1, frame_stack.last_mut().unwrap().stack.pop().unwrap());
        assert_eq!(None, frame_stack.last_mut().unwrap().stack.pop());
    }

    #[test]
    fn test_op_istore() {
        // create test method with ISTORE, 0
        let test_method = &ResolvedMethod::test_with_code(&[jvm::ISTORE, 0]);
        let frame_stack = &mut vec![Frame::new(test_method)];

        // setup stack 42
        frame_stack.last_mut().unwrap().stack.push(42);

        // Stack is as expected
        assert_eq!(vec![42], frame_stack.last().unwrap().stack);

        // locals are not set
        assert_eq!(0, frame_stack.last_mut().unwrap().locals[0]);

        // pc is 0
        assert_eq!(0, frame_stack.last().unwrap().pc);

        let result = op_istore(frame_stack);

        assert_eq!(None, result);

        // pc is 2
        assert_eq!(2, frame_stack.last().unwrap().pc);

        // empty stack
        assert!(frame_stack.last().unwrap().stack.is_empty());

        // local[0] is set
        assert_eq!(42, frame_stack.last().unwrap().locals[0]);
    }

    #[test]
    fn test_op_istore_i() {
        let test_method = &ResolvedMethod::test();
        let frame_stack = &mut vec![Frame::new(test_method)];

        frame_stack.last_mut().unwrap().stack.push(42);
        frame_stack.last_mut().unwrap().stack.push(41);
        frame_stack.last_mut().unwrap().stack.push(40);

        // Stack is as expected
        assert_eq!(vec![42, 41, 40], frame_stack.last().unwrap().stack);

        // locals are not set
        assert_eq!(0, frame_stack.last_mut().unwrap().locals[0]);
        assert_eq!(0, frame_stack.last_mut().unwrap().locals[1]);
        assert_eq!(0, frame_stack.last_mut().unwrap().locals[2]);

        // pc is 0
        assert_eq!(0, frame_stack.last().unwrap().pc);

        // store from stack
        let result = op_istore_i(frame_stack, 0);
        assert_eq!(result, None);

        // pc is 1
        assert_eq!(1, frame_stack.last().unwrap().pc);

        op_istore_i(frame_stack, 1);

        // pc is 2
        assert_eq!(2, frame_stack.last().unwrap().pc);

        op_istore_i(frame_stack, 2);

        // pc is 3
        assert_eq!(3, frame_stack.last().unwrap().pc);

        // locals are set
        assert_eq!(40, frame_stack.last_mut().unwrap().locals[0]);
        assert_eq!(41, frame_stack.last_mut().unwrap().locals[1]);
        assert_eq!(42, frame_stack.last_mut().unwrap().locals[2]);

        // ... and stack is empty
        assert!(frame_stack.last().unwrap().stack.is_empty());
    }

    #[test]
    fn test_op_iload() {
        // create test method with ILOAD, 0
        let test_method = &ResolvedMethod::test_with_code(&[jvm::ILOAD, 0]);
        let frame_stack = &mut vec![Frame::new(test_method)];

        // set locals
        frame_stack.last_mut().unwrap().locals[0] = 40;

        // locals are set
        assert_eq!(40, frame_stack.last_mut().unwrap().locals[0]);

        // pc is 0
        assert_eq!(0, frame_stack.last().unwrap().pc);

        let result = op_iload(frame_stack);

        assert_eq!(None, result);

        // pc is 2
        assert_eq!(2, frame_stack.last().unwrap().pc);

        // local[0] was loaded to stack
        assert_eq!(vec![40], frame_stack.last().unwrap().stack);
    }

    #[test]
    fn test_op_iload_i() {
        let test_method = &ResolvedMethod::test();
        let frame_stack = &mut vec![Frame::new(test_method)];

        frame_stack.last_mut().unwrap().locals[0] = 40;
        frame_stack.last_mut().unwrap().locals[1] = 41;
        frame_stack.last_mut().unwrap().locals[2] = 42;

        // locals are set
        assert_eq!(40, frame_stack.last_mut().unwrap().locals[0]);
        assert_eq!(41, frame_stack.last_mut().unwrap().locals[1]);
        assert_eq!(42, frame_stack.last_mut().unwrap().locals[2]);

        // empty stack
        assert!(frame_stack.last().unwrap().stack.is_empty());

        // pc is 0
        assert_eq!(0, frame_stack.last().unwrap().pc);

        // load locals
        let result = op_iload_i(frame_stack, 0);
        assert_eq!(result, None);

        // pc is 1
        assert_eq!(1, frame_stack.last().unwrap().pc);

        op_iload_i(frame_stack, 1);

        // pc is 2
        assert_eq!(2, frame_stack.last().unwrap().pc);

        op_iload_i(frame_stack, 2);

        // pc is 3
        assert_eq!(3, frame_stack.last().unwrap().pc);

        // locals were loaded to stack
        assert_eq!(vec![40, 41, 42], frame_stack.last().unwrap().stack);
    }

    #[test]
    fn test_op_iadd() {
        let test_method = &ResolvedMethod::test();
        let frame_stack = &mut vec![Frame::new(test_method)];

        // setup stack 1, 2
        frame_stack.last_mut().unwrap().stack.push(1);
        frame_stack.last_mut().unwrap().stack.push(2);

        // Stack is as expected
        assert_eq!(vec![1, 2], frame_stack.last().unwrap().stack);

        // pc is 0
        assert_eq!(0, frame_stack.last().unwrap().pc);

        // Add
        let result = op_iadd(frame_stack);
        assert_eq!(None, result);

        // pc is 1
        assert_eq!(1, frame_stack.last().unwrap().pc);

        // Result is now on stack
        assert_eq!(vec![3], frame_stack.last().unwrap().stack);

        // push to stack stack -1, -2
        frame_stack.last_mut().unwrap().stack.push(-1);
        frame_stack.last_mut().unwrap().stack.push(-2);

        // Stack is as expected
        assert_eq!(vec![3, -1, -2], frame_stack.last().unwrap().stack);

        // Add
        op_iadd(frame_stack);

        // pc is 2
        assert_eq!(2, frame_stack.last().unwrap().pc);

        // Result is now on stack
        assert_eq!(vec![3, -3], frame_stack.last().unwrap().stack);

        // push to stack stack 0
        frame_stack.last_mut().unwrap().stack.push(0);

        // Stack is as expected
        assert_eq!(vec![3, -3, 0], frame_stack.last().unwrap().stack);

        // Add
        op_iadd(frame_stack);

        // pc is 3
        assert_eq!(3, frame_stack.last().unwrap().pc);

        // Result is now on stack
        assert_eq!(vec![3, -3], frame_stack.last().unwrap().stack);

        // Add
        op_iadd(frame_stack);

        // pc is 4
        assert_eq!(4, frame_stack.last().unwrap().pc);

        // Result is now on stack
        assert_eq!(vec![0], frame_stack.last().unwrap().stack);
    }

    #[test]
    fn test_op_ireturn() {
        let test_method = &ResolvedMethod::test();

        let frame_stack = &mut vec![Frame::new(test_method)];

        // Caller stack is empty
        assert!(frame_stack.last().unwrap().stack.is_empty());

        // Caller pc is 0
        assert_eq!(0, frame_stack.last().unwrap().pc);

        // push callee frame
        frame_stack.push(Frame::new(test_method));

        assert_eq!(2, frame_stack.len());

        // setup value to be returned
        frame_stack.last_mut().unwrap().stack.push(5);

        let result = op_ireturn(frame_stack);
        assert_eq!(None, result);

        // callee frame was popped
        assert_eq!(1, frame_stack.len());

        // Caller pc is 0 (unchanged, caller responsible for setting pc _before_ caller was called)
        assert_eq!(0, frame_stack.last().unwrap().pc);

        // Value was pushed to caller stack
        assert_eq!(vec![5], frame_stack.last().unwrap().stack);

        // return without caller returns value to rust...
        let returned_value = op_ireturn(frame_stack);
        assert_eq!(Some(5), returned_value);

        // ... and frame stack is empty
        assert!(frame_stack.is_empty());
    }
}
