/*
 * java_vm - A java-like virtual machine in Rust.
 *
 * Copyright (c)2018 Ross Bamford <roscopeco AT gmail DOT com>
 */
extern crate classfile_parser;

pub mod loader;
pub mod interpreter;
