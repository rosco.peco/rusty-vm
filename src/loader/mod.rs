/*
 * java_vm loader
 *
 * Copyright (c)2018 Ross Bamford <roscopeco AT gmail DOT com>
 */
use classfile_parser::ClassFile;
use classfile_parser::method_info::MethodInfo;
use classfile_parser::constant_info::ConstantInfo;
use classfile_parser::attribute_info::code_attribute_parser;

#[derive(Debug, PartialEq)]
pub struct ResolvedClass {
    pub name: String,
    pub super_name: String,
    pub methods: Vec<ResolvedMethod>,
}

impl ResolvedClass {
    pub fn resolve(class_file: &ClassFile) -> ResolvedClass {
        resolve_class(class_file)
    }

    pub fn find_method(&self, name: &String, descriptor: &String) -> Option<&ResolvedMethod> {
        for method in &self.methods {
            if method.name.eq(name) && method.descriptor.eq(descriptor) {
                return Some(method);
            }
        }

        None
    }
}

#[derive(Debug, PartialEq)]
pub struct ResolvedMethod {
    pub name: String,
    pub descriptor: String,
    pub code: Vec<u8>,
}

impl ResolvedMethod {
    pub fn resolve(class_file: &ClassFile, method_info: &MethodInfo) -> ResolvedMethod {
        resolve_method(method_info, &class_file.const_pool)
    }

    #[cfg(test)]
    pub fn test() -> ResolvedMethod {
        ResolvedMethod {
            name: "test".to_string(),
            descriptor: "()V".to_string(),
            code: vec![0xb1]
        }
    }

    #[cfg(test)]
    pub fn test_with_code(code : &[u8]) -> ResolvedMethod {
        ResolvedMethod {
            name: "test".to_string(),
            descriptor: "()V".to_string(),
            code: code.to_vec(),
        }
    }
}

fn get_constant_utf8(idx: u16, pool: &[ConstantInfo]) -> Result<String, String> {
    let s = &pool[(idx - 1) as usize];

    match *s {
        ConstantInfo::Utf8(ref utf8) => Ok(utf8.utf8_string.clone()),
        _ => Err(format!("Pool index {} is not a UTF8 constant", idx))
    }
}

fn get_constant_class(idx: u16, pool: &[ConstantInfo]) -> Result<String, String> {
    let s = &pool[(idx - 1) as usize];

    match *s {
        ConstantInfo::Class(ref class) => get_constant_utf8(class.name_index, pool),
        _ => Err(format!("Pool index {} is not a CLASS constant", idx))
    }
}

fn resolve_class(class: &ClassFile) -> ResolvedClass {
    ResolvedClass {
        name: get_constant_class(class.this_class, &class.const_pool).expect("Malformed class"),
        super_name: get_constant_class(class.super_class, &class.const_pool).expect("Malformed class"),
        methods: resolve_methods(class),
    }
}

fn get_method_code(method_info: &MethodInfo, pool: &[ConstantInfo]) -> Vec<u8> {
    for attribute in &method_info.attributes {
        match get_constant_utf8(attribute.attribute_name_index, pool).unwrap().as_ref() {
            "Code" => return code_attribute_parser(&attribute.info).unwrap().1.code,
            _ => println!("Unknown attribute"),
        }
    }

    // Method is abstract/native
    Vec::new()
}

fn resolve_method(method_info: &MethodInfo, pool: &[ConstantInfo]) -> ResolvedMethod {
    ResolvedMethod {
        name: get_constant_utf8(method_info.name_index, pool).expect("Malformed class"),
        descriptor: get_constant_utf8(method_info.descriptor_index, pool).expect("Malformed class"),
        code: get_method_code(method_info, pool),
    }
}

fn resolve_methods(class_file: &ClassFile) -> Vec<ResolvedMethod> {
    let mut vec = Vec::new();

    for method_info in &class_file.methods {
        vec.push(ResolvedMethod::resolve(class_file, method_info));
    }

    vec
}

pub fn dump_class(class: &ClassFile) {
    println!("Version is: {}.{}", class.major_version, class.minor_version);
    println!("Constant pool has {} entries", class.const_pool_size);

    let mut idx = 1;

    for pool_entry in &class.const_pool {
        match *pool_entry {
            ConstantInfo::Utf8              (ref s) => println!("{:05}: Utf 8              : {}", idx, s.utf8_string),
            ConstantInfo::Integer           (ref i) => println!("{:05}: Integer            : {}", idx, i.value),
            ConstantInfo::Float             (ref f) => println!("{:05}: Float              : {}", idx, f.value),
            ConstantInfo::Long              (ref j) => println!("{:05}: Long               : {}", idx, j.value),
            ConstantInfo::Double            (ref d) => println!("{:05}: Long               : {}", idx, d.value),
            ConstantInfo::Class             (ref c) => println!("{:05}: Class              : {}", idx, c.name_index),
            ConstantInfo::String            (ref s) => println!("{:05}: String             : {}", idx, s.string_index),
            ConstantInfo::FieldRef          (ref f) => println!("{:05}: FieldRef           : {}.{}", idx, f.class_index, f.name_and_type_index),
            ConstantInfo::MethodRef         (ref m) => println!("{:05}: MethodRef          : {}.{}", idx, m.class_index, m.name_and_type_index),
            ConstantInfo::InterfaceMethodRef(ref m) => println!("{:05}: InterfaceMethodRef : {}.{}", idx, m.class_index, m.name_and_type_index),
            ConstantInfo::NameAndType       (ref n) => println!("{:05}: NameAndType        : {}, {}", idx, n.name_index, n.descriptor_index),
            ConstantInfo::MethodHandle      (ref h) => println!("{:05}: MethodHandle       : {}, {}", idx, h.reference_kind, h.reference_index),
            ConstantInfo::MethodType        (ref t) => println!("{:05}: MethodType         : {}", idx, t.descriptor_index),
            ConstantInfo::InvokeDynamic     (ref i) => println!("{:05}: InvokeDynamic      : {}, {}", idx, i.bootstrap_method_attr_index, i.name_and_type_index),
            ConstantInfo::Unusable                  => println!("{:05}: UNUSABLE", idx),
        }

        idx += 1;
    }

    println!("There are {} method(s)", class.methods_count);

    for method_entry in &class.methods {
        println!("Name      : {}", method_entry.name_index);
        println!("Descriptor: {}", method_entry.descriptor_index);
        println!("AttrCount : {}", method_entry.attributes_count);
    }
}
