/*
 * java_vm integration tests
 *
 * Copyright (c)2018 Ross Bamford <roscopeco AT gmail DOT com>
 */
extern crate classfile_parser;
extern crate java_vm;

#[cfg(test)]
mod tests {
    use classfile_parser::parse_class;
    use java_vm::loader::ResolvedClass;
    use java_vm::interpreter::Interpreter;

    #[test]
    fn resolve_class_basics() {
        let resolved_class = ResolvedClass::resolve(&parse_class("java/bin/Test").unwrap());

        assert_eq!(resolved_class.name, "Test");
        assert_eq!(resolved_class.super_name, "java/lang/Object");
        assert_eq!(resolved_class.methods.len(), 2);

        assert_eq!(resolved_class.methods[0].name, "<init>");
        assert_eq!(resolved_class.methods[0].descriptor, "()V");

        assert_eq!(resolved_class.methods[1].name, "test");
        assert_eq!(resolved_class.methods[1].descriptor, "()I");
    }

    #[test]
    fn resolved_class_find_method() {
        let resolved_class = ResolvedClass::resolve(&parse_class("java/bin/Test").unwrap());

        let mut resolved_method = resolved_class.find_method(&"<init>".to_string(), &"()V".to_string()).unwrap();

        assert_eq!(resolved_method.name, "<init>");
        assert_eq!(resolved_method.descriptor, "()V");

        resolved_method = resolved_class.find_method(&"test".to_string(), &"()I".to_string()).unwrap();

        assert_eq!(resolved_method.name, "test");
        assert_eq!(resolved_method.descriptor, "()I");

        let nosuch_method = resolved_class.find_method(&"doesntExist".to_string(), &"()J".to_string());

        assert_eq!(None, nosuch_method);
    }

    #[test]
    fn test_interpreter_basics() {
        // tests ICONST_1, ICONST_2, ISTORE_1, ISTORE_2, ILOAD_1, ILOAD_2, IADD, IRETURN
        let resolved_class = ResolvedClass::resolve(&parse_class("java/bin/Test").unwrap());
        let result = Interpreter::run_to_result(&resolved_class.methods[1]).unwrap();

        assert_eq!(result, 3);
    }
}
